package b_intermediarios;

import java.util.Random;
/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	public static void main(String args[]) {
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
		
		Random random = new Random();
		int sorteio1 = random.nextInt(5);
		int sorteio2 = random.nextInt(5);
		int sorteio3 = random.nextInt(5);
		
		int pontos = 0;
		
		if(valores[sorteio1].equals(valores[sorteio2]) && valores[sorteio2].equals(valores[sorteio3])) {
			if(valores[sorteio1] != "7") {
				pontos = 1000;
			}
			else {
				pontos = 5000;
			}
		}
		else {
			if(valores[sorteio1].equals(valores[sorteio2]) || valores[sorteio2].equals(valores[sorteio3]) || valores[sorteio1].equals(valores[sorteio3])){
				pontos = 100;
			}
		}
		
		System.out.println(valores[sorteio1] + " " + valores[sorteio2] + " " + valores[sorteio3]);
		if(pontos == 0) {
			System.out.println("0 ponto!");
		}else {
			System.out.println(pontos + " pontos!");
		}
	}
}
