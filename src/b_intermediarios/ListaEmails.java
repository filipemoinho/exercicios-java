package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior à 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {
		
		Path path = Paths.get("bin/contas.csv");
		Path patharq = Paths.get("Clientes_Acima.txt");
		String umaString = "";
		
		try {
			
			List<String> lista = Files.readAllLines(path);
			lista.remove(0);
			
			// Exemplo 1 de exibição!
//			for (int i = 0; i < lista.size(); i++) {
//				System.out.println(lista.get(i));
//			}
			
//			// Exemplo 2 de exibição!
//			for (String linha: lista) {
//				System.out.println(linha);
//			}
			
			// Exemplo 3 de exibição!
//			lista.forEach(linha -> System.out.println(linha));
			
			for (String linha: lista) {
				String[] partes = linha.split(",");
					
					int saldo = Integer.parseInt(partes[4]);
					
				if (saldo > 7000) {
					umaString += partes[1] + " " + partes[2] + "<" + partes[3] + ">, ";
					//System.out.println(partes[1]);
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(umaString);
		
		try {
			Files.write(patharq,  umaString.getBytes());
		} catch(IOException e) {
			e.printStackTrace();
		}	
		
		
		
	}

	private static int Int(String string) {
		// TODO Auto-generated method stub
		return 0;
	}

	private static int Number(String string) {
		// TODO Auto-generated method stub
		return 0;
	}
}
