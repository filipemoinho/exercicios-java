
package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.nio.file.Path;
/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		Path path = Paths.get("teste.txt");
		String umaString = "";
		System.out.println("Digite a lista de tarefas ou 0 para sair ");
		Scanner scanner = new Scanner(System.in);
		
		String entrada = scanner.nextLine();
		char zer = entrada.charAt(entrada.length() - 1);
		while(zer != '0'){
			umaString += entrada + "\n";
			System.out.println("Digite a tarefa ou 0 para sair ");
			entrada = scanner.next();
			zer = entrada.charAt(entrada.length() - 1);
		}
		
		try {
			Files.write(path,  umaString.getBytes());
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}