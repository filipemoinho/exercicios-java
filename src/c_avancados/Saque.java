package c_avancados;

import java.io.IOException; 
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {
		Path path = Paths.get("src/contasSaque.csv");
		List<String> lista = lerContas();

		try {

			String cabecalho = lista.get(0);
			lista.remove(0);

			System.out.println("Digite a sua conta ");
			Scanner scanner = new Scanner(System.in);
			int contaInformada = scanner.nextInt();
			String contaValidada = "";


			for (int i = 0; i < lista.size(); i++) {
				String linha = lista.get(i);
				String[] partes = linha.split(",");

				int Conta = Integer.parseInt(partes[0]);
				int saldo = Integer.parseInt(partes[4]);

				if (Conta == contaInformada) {
					contaValidada = "OK";
					System.out.println("Digite Quanto você quer sacar: ");
					Scanner scannerValor = new Scanner(System.in);
					int valorSaque = scanner.nextInt();

					if (valorSaque > saldo) {
						System.out.println("Valor acima do saldo existente. Saldo Atual:" + saldo);
						contaValidada = "NOK";
					}
					else {
						saldo = saldo - valorSaque;
						System.out.println("Saque realizado! Saldo atual: " + saldo);
						String novaLinha = "";
						partes[4] = Integer.toString(saldo);

						for(String item: partes) {
							if (item == partes[4]) {
								novaLinha += item;
							}
							else {
								novaLinha += item + ",";
							}
						}

						lista.set(i, novaLinha);
					}

				}
			}

			if (contaValidada == "") {
				System.out.println("Conta invalida");
			} else
			{

				String novaLinha = cabecalho + "\n";

				for ( int u = 0; u < lista.size(); u++) {

					String linha = lista.get(u);
					novaLinha += linha + "\n";

				}


				Files.write(path,  novaLinha.getBytes());
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public static List<String> lerContas(){
		Path path = Paths.get("src/contasSaque.csv");

		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}


		return lista;
	}


}
